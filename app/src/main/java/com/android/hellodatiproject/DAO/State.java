package com.android.hellodatiproject.DAO;

import android.util.Log;

import com.android.hellodatiproject.SSAPI.SSAPI;
import com.android.hellodatiproject.SSAPI.SSAPI_PARAMS;


/**
 * © Kamatcho, 04/07/2020 -  My Application.
 */
public class State {
    private final String path  = "/State/";

    public void getAll(String language, String executeOnsuccess, String executeOnerror) {
        SSAPI_PARAMS param = new SSAPI_PARAMS();
        param.onSuccess = executeOnsuccess;
        param.onError = executeOnerror;
        param.URI = this.path+"getAll";
        SSAPI.getInstance().submit(param.getJson());
    }

    public void getId(String language, Integer idState, String executeOnsuccess, String executeOnerror){
        Log.d("khalil request id : ", ""+idState);
        SSAPI_PARAMS param = new SSAPI_PARAMS();
        param.addPost("id",idState.toString());
        param.addGets("lan",language);
        param.onSuccess = executeOnsuccess;
        param.onError = executeOnerror;
        param.URI = this.path+"getId";
        SSAPI.getInstance().submit(param.getJson());
    }
}
