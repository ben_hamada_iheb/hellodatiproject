package com.android.hellodatiproject.SSAPI.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Tourist {
    @SerializedName("id")
    @Expose
    private Integer id = -1;
    @SerializedName("cin_number")
    @Expose
    private Integer cinNumber;
    @SerializedName("passport_number")
    @Expose
    private String passportNumber;
    @SerializedName("first_name")
    @Expose
    private String firstName = "";
    @SerializedName("last_name")
    @Expose
    private String lastName = "";
    @SerializedName("password")
    @Expose
    private String pwd = "";
    @SerializedName("prefix_name")
    @Expose
    private String prefixName = "";
    @SerializedName("born")
    @Expose
    private String born;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("languages")
    @Expose
    private List<String> languages = Arrays.asList();
    ;
    @SerializedName("country")
    @Expose
    private String country = "";
    @SerializedName("city")
    @Expose
    private String city = "";
    @SerializedName("zip_code")
    @Expose
    private int zipCode;
    @SerializedName("address_1")
    @Expose
    private String address1 = "";
    @SerializedName("address_2")
    @Expose
    private String address2 = "";
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("email_status")
    @Expose
    private Integer emailStatus;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber = "";
    @SerializedName("work_phone_number")
    @Expose
    private String workPhoneNumber = "";
    @SerializedName("company")
    @Expose
    private String company = "";
    @SerializedName("is_resident")
    @Expose
    private Boolean isResident;
    @SerializedName("app_lang")
    @Expose
    private String appLang;

    File file;


    public Tourist(File file) {
        this.file = file;
    }

    public Tourist() {
    }

    public Tourist(Integer id, String firstName, String lastName, String prefixName, String born, Integer gender, List<String> languages, String country, String city, int zipCode, String address1, String address2, String email, String phoneNumber, String workPhoneNumber, String company, Boolean isResident) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.prefixName = prefixName;
        this.born = born;
        this.gender = gender;
        this.languages = languages;
        this.country = country;
        this.city = city;
        this.zipCode = zipCode;
        this.address1 = address1;
        this.address2 = address2;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.workPhoneNumber = workPhoneNumber;
        this.company = company;
        this.isResident = isResident;
    }

    public Tourist(String pwd, String email, String phoneNumber) {
        this.pwd = pwd;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Tourist(String pwd, String email, String phoneNumber, Object image) {
        this.pwd = pwd;
        this.email = email;
        this.image = image;
        this.phoneNumber = phoneNumber;
    }

    public Tourist(Object image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCinNumber() {
        return cinNumber;
    }

    public void setCinNumber(Integer cinNumber) {
        this.cinNumber = cinNumber;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPwd() {
        return pwd;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Boolean getResident() {
        return isResident;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getAppLang() {
        return appLang;
    }

    public void setAppLang(String appLang) {
        this.appLang = appLang;
    }
}
