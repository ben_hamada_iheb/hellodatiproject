package com.android.hellodatiproject.SSAPI;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

abstract class SSAPI_MANAGER extends AppCompatActivity {

    private String SESSION = "new";
    private String AID = "10928344384930";
    //app secret
    private String AS = "DatiskjdsnDSQI4524dsqkdsls$dioesp";
    //
    private String SI;
    private String SAT;
    private String SRT;
    protected boolean wait = false;
    private boolean waitForpile = false;
    private String DNS = "https://api2.sowazi.com";
    protected List<String> pile = new ArrayList<>();
    protected String lastRequest_param="";
    protected String getDNS() {
        return DNS;
    }

    protected String getSI() {
        return SI;
    }

    protected String getSAT() {
        return SAT;
    }

    protected String getSRT() {
        return SRT;
    }

    protected String getSESSION() {
        return SESSION;
    }

    protected String getAID() {
        return AID;
    }

    protected String getAS() {
        return AS;
    }

    protected boolean isWait() {
        return wait;
    }

    public void setWait(boolean wait) {
        this.wait = wait;
    }

    public void update(Object session) {
        //this.SI = session.id;
        this.SAT = SAT;

    }

    protected Boolean EXIST_IN_PILE(String param){
        if(this.lastRequest_param.equals(param)){
            return true;
        }
        for(int i=0;i<this.pile.size();i++){
            if(this.pile.get(0).equals(param)){
                return true;
            }
        }
        return false;
    }
    protected void RESEND_with_REFRESH(String param) {
        if (this.wait == true) {
            Integer length = this.pile.size();
            if (!this.EXIST_IN_PILE(param)) {
                Log.d("khalil2 refresh Add to pile",param);
                this.pile.add(param);
            }else{
                Log.e("khalil2 refresh always exist in pile",param);
            }
        } else {
            this.SESSION = "refresh";
            SSAPI.getInstance().submit(param);
            this.wait = true;
            this.lastRequest_param = param;
        }
    }

    protected void RESEND_with_NEW(String param) {
        if (this.wait == true) {
            Integer length = this.pile.size();
            if (!this.EXIST_IN_PILE(param)) {
                this.pile.add(param);
            }
        } else {
            this.SESSION = "new";
            SSAPI.getInstance().submit(param);
            this.wait = true;
            this.lastRequest_param = param;
        }
    }

    private void EXEC_PILE(){
        while(this.pile.size() > 0){
            SSAPI.getInstance().submit(this.pile.get(0));
            Log.d("khalil2 exec pile", "pile param"+this.pile.get(0));
            this.pile.remove(0);
        }
    }
    protected void update_params(JSONObject s) {
        try {
            this.SI = s.getString("id");
            this.SAT = s.getString("access_token");
            this.SRT = s.getString("refresh_token");
            this.SESSION = "linked";
            this.wait = false;
            this.lastRequest_param="";
            this.EXEC_PILE();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    protected Boolean SESSION_ERROR(int erroCode) {
        if ((erroCode >= 1100) && (erroCode <= 1120)) {
            return true;
        } else {
            return false;
        }
    }

    protected Boolean SESSION_NEED_REFRESH(int erroCode) {
        if (erroCode == 1115 || erroCode == 1120) {
            return true;
        } else {
            return false;
        }
    }


    protected Boolean SESSION_REOPEN(int erroCode) {
        if (erroCode == 1113 || erroCode == 1114 || erroCode == 1116 || erroCode == 1119 || erroCode == 1104) {
            return true;
        } else {
            return false;
        }
    }

    protected Boolean SESSION_STOPED_JAVA(int erroCode) {
        if (erroCode == 1100 || erroCode == 1101 || erroCode == 1102 || erroCode == 1103 || erroCode == 1105 || erroCode == 1109 || erroCode == 1107 || erroCode == 1112) {
            return true;
        } else {
            return false;
        }
    }

    protected Boolean SESSION_STOPED_SERVER(int erroCode) {
        if (erroCode == 1106 || erroCode == 1108 || erroCode == 1110 || erroCode == 1119 || erroCode == 1111) {
            return true;
        } else {
            return false;
        }
    }

    protected Boolean FALSE_REFRESH(int erroCode) {

        if (erroCode == 1117) {
            return true;
        } else {
            return false;
        }
    }

    protected void RESOLVE_SESSION(String param, int erroCode) {
        Log.d("khalil2 khalil3 lasr error code", ""+erroCode);
        //Param URl with data & onsuccess & onError & data
        if (this.SESSION_NEED_REFRESH(erroCode)) {
            // resend with refresh session
            this.RESEND_with_REFRESH(param);
        } else if (this.SESSION_REOPEN(erroCode)) {
            // resend with new session
            // if is auth send authentification
            this.RESEND_with_NEW(param);
        } else if (this.SESSION_STOPED_JAVA(erroCode)) {
            // stop session and block all request
            // javascript error configuration
            Log.e("Error code : " + erroCode + " -> verify configuration", "Error Java");
        } else if (this.SESSION_STOPED_SERVER(erroCode)) {
            // stop session and block all request
            // javascript error configuration
            Log.e("Error code : " + erroCode + " -> verify configuration", "Error Server");
        }
    }


}
