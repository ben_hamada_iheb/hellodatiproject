package com.android.hellodatiproject.UI;

import android.graphics.drawable.Drawable;

public class AppInfo {
    public String label;
    public String packageName;
    public Drawable icon;
}
