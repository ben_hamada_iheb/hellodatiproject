package com.android.hellodatiproject.UI;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.hellodatiproject.R;
import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.SSAPI.Model.Hotel;
import com.ncorti.slidetoact.SlideToActView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

public class UI_IntroActivity extends AppCompatActivity {

    Activity activity;
    public static ImageView hotelImg,hotelLogo;
    public static TextView hotelTitle;
    public static EditText userId,userIdCnfr;
    public static RatingBar hotelStars;
    public static SlideToActView btnSuivant;
    public static Hotel hotel;
    public static State stateDAO=new State();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__intro);
        activity=this;
        // init layout
        initContent();
        btnSuivant.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(@NotNull SlideToActView slideToActView) {
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
            }
        });

        btnSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userId.getText().toString().length() == 4) {
                    if (userIdCnfr.getText().toString().equals(userId.getText().toString())) {
                        btnSuivant.setLocked(false);
                        //set user Id
                        /*assert HelloDatiApplication.get() != null;
                        HelloDatiApplication.get().getHelloDatiService().fetchDevice()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new MyDisposableObserver<DevicesResponse>() {
                                    @Override
                                    public void onNext(DevicesResponse devicesResponse) {
                                        if (devicesResponse.getData().size() > 0) {
                                            int touristId = devicesResponse.getData().get(0).getDeviceRoom().getStay().getTourist().getId();
                                            HelloDatiApplication.get().getHelloDatiService().fetchTourist(touristId)
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribeOn(Schedulers.io())
                                                    .subscribe(new MyDisposableObserver<TouristsResponse>() {
                                                        @Override
                                                        public void onUnhandledError(Throwable e) {
                                                        }

                                                        @Override
                                                        public void onNext(TouristsResponse touristsResponse) {
                                                            Tourist t = new Tourist();
                                                            // set the user code to the tourist connected
                                                            HelloDatiApplication.get().getHelloDatiService().updateTourist(touristsResponse.getData().get(0).getId(), t, bindingView.userId.getText().toString())
                                                                    .observeOn(AndroidSchedulers.mainThread())
                                                                    .subscribeOn(Schedulers.io())
                                                                    .subscribe(new MyDisposableObserver<TouristsResponse>() {
                                                                        @Override
                                                                        public void onNext(TouristsResponse touristsResponse) {
                                                                            // go to the main page
                                                                            bindingView.suivantBtn.setLocked(false);
                                                                        }

                                                                        @Override
                                                                        public void onComplete() {
                                                                        }

                                                                        @Override
                                                                        public void onUnhandledError(Throwable e) {
                                                                            if(bindingView.suivantBtn.isLocked()==true){
                                                                                AlertDialog.Builder builder;
                                                                                builder = new AlertDialog.Builder(activity);
                                                                                builder//.setTitle(R.string.setup_title)
                                                                                        .setMessage("cannot connect to server!")
                                                                                        .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                                                                            public void onClick(DialogInterface dialog, int which) {
                                                                                            }
                                                                                        })
                                                                                        .setNeutralButton("Wifi", new DialogInterface.OnClickListener() {
                                                                                            public void onClick(DialogInterface dialog, int id) {
                                                                                                activity.getParent().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                                                                            }
                                                                                        })
                                                                                        //.setIcon(android.R.drawable.ic_dialog_alert)
                                                                                        .setCancelable(false)
                                                                                        .show();
                                                                                Toast.makeText(activity.getApplicationContext(),activity.getString(R.string.verify_connexion),Toast.LENGTH_LONG).show();
                                                                            }
                                                                        }
                                                                    });
                                                        }

                                                        @Override
                                                        public void onComplete() {

                                                        }
                                                    });
                                        }
                                    }

                                    @Override
                                    public void onComplete() {
                                    }

                                    @Override
                                    public void onUnhandledError(Throwable e) {
                                        if(bindingView.suivantBtn.isLocked()==true){
                                            AlertDialog.Builder builder;
                                            builder = new AlertDialog.Builder(activity);
                                            builder//.setTitle(R.string.setup_title)
                                                    .setMessage("cannot connect to server!")
                                                    .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                        }
                                                    })
                                                    .setNeutralButton("Wifi", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            activity.getParent().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                                        }
                                                    })
                                                    //.setIcon(android.R.drawable.ic_dialog_alert)
                                                    .setCancelable(false)
                                                    .show();
                                            Toast.makeText(activity.getApplicationContext(),activity.getString(R.string.verify_connexion),Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });*/
                    } else {
                        userIdCnfr.requestFocus();
                        Toast.makeText(activity, activity.getResources().getString(R.string.user_id_cnfr_required), Toast.LENGTH_LONG).show();
                        
                    }
                } else {
                    userId.requestFocus();
                    Toast.makeText(activity, activity.getResources().getString(R.string.user_id_required), Toast.LENGTH_LONG).show();
                }
            }
        });

        stateDAO.getAll("fr","iIntro.getData","iIntro.getDataError");
        doTask send=new doTask();
        send.execute();
    }

    public static class doTask extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
        }


        protected String doInBackground(Void... urls) {
            return null;
        }


        protected void onPostExecute(String response) {
            setData();
        }
    }

    public static void setData(){

    }



    public void initContent(){

        hotelImg=findViewById(R.id.hotel_image);
        hotelLogo=findViewById(R.id.hotel_logo);
        hotelTitle=findViewById(R.id.hotel_title);
        userId=findViewById(R.id.user_id);
        userIdCnfr=findViewById(R.id.user_id_cnfr);
        hotelStars=findViewById(R.id.hotel_stars);
        btnSuivant=findViewById(R.id.btn_suivant);
    }
}
