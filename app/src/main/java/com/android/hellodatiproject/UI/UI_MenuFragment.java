package com.android.hellodatiproject.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_MenuFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView categoriesRecycler,contentRecycler;
    public static TextView choosenContentName,choosenContentPrice,choosenContentSummery,choosenContentDescription;
    public static FrameLayout choosenContentLayout;
    public static CardView btnOrder,btnCancel;

    public static State stateDAO=new State();

    public UI_MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);

        stateDAO.getAll("fr","iMenuAdapter.getCategoriesData","iMenuAdapter.getCategoriesDataError");
        stateDAO.getAll("fr","iMenuAdapter.getContentData","iMenuAdapter.getContentDataError");

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosenContentLayout.setVisibility(View.GONE);
            }
        });
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add choosen content to basket
                choosenContentLayout.setVisibility(View.GONE);
            }
        });

        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui_menu, container, false);
        categoriesRecycler=view.findViewById(R.id.categories_recycler);
        contentRecycler=view.findViewById(R.id.content_recycler);
        choosenContentName=view.findViewById(R.id.choosen_content_name);
        choosenContentPrice=view.findViewById(R.id.choosen_content_price);
        choosenContentSummery=view.findViewById(R.id.choosen_content_summery);
        choosenContentDescription=view.findViewById(R.id.choosen_content_description);
        choosenContentLayout=view.findViewById(R.id.choosen_content_layout);
        btnOrder=view.findViewById(R.id.btn_order);
        btnCancel=view.findViewById(R.id.btn_cancel);
    }

}
