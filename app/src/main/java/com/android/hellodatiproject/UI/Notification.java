package com.android.hellodatiproject.UI;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

import java.util.Locale;

public class Notification extends AppCompatActivity {

    public static RelativeLayout emptyNotification,btnBack,relativeLayout;
    public static RecyclerView recyclerView;
    public static Context context;
    public static State stateDAO=new State();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getApplicationContext();
        Intent intent=getIntent();
        String lang="en";
        Locale locale1 = null;
        if(intent!=null){
            lang=intent.getStringExtra("Lang");
            if(lang==null){
                locale1 = new Locale("en");
            }
            else {
                locale1 = new Locale(lang);
            }
            Locale.setDefault(locale1);
            Configuration config = new Configuration();
            config.locale = locale1;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        initContent();
        relativeLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_FULLSCREEN);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        stateDAO.getAll("fr","iNotification.getNotificationListData","iNotification.getNotificationListDataError");
    }

    private void initContent() {
        setContentView(R.layout.activity_notification);
        recyclerView=findViewById(R.id.recycler_view);
        emptyNotification=findViewById(R.id.empty_notification);
        btnBack=findViewById(R.id.btn_back);
        relativeLayout=findViewById(R.id.relative_layout);
    }
}
