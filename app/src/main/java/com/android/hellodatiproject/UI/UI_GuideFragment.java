package com.android.hellodatiproject.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_GuideFragment extends Fragment {
    public static View view;
    public static Context context;
    public static State stateDAO=new State();

    public UI_GuideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initContent(inflater,container,savedInstanceState);
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__guide, container, false);

    }
}
