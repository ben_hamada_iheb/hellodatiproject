package com.android.hellodatiproject.UI;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.hellodatiproject.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_BookingFragment extends Fragment {

    public View view;
    public static Context context;
    public static ImageButton increase,decrease;
    public static EditText nbrPerson,comment;
    public static CardView btnBook,date,hour;
    public static TextView dateTxt,hourTxt;
    public static RelativeLayout bookingLayout;
    public static int nbr;

    Calendar myCalendar = Calendar.getInstance(Locale.FRANCE);

    public UI_BookingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        nbr=1;
        nbrPerson.setText(nbr+"");
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nbr=Integer.parseInt(nbrPerson.getText().toString());
                nbr++;
                nbrPerson.setText(nbr+"");
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nbr>1){
                    nbr=Integer.parseInt(nbrPerson.getText().toString());
                    nbr--;
                    nbrPerson.setText(nbr+"");
                }
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDatePick(v);
            }
        });
        hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTimePick(v);
            }
        });
        bookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui_booking, container, false);
        nbrPerson=view.findViewById(R.id.nbr_person);
        increase=view.findViewById(R.id.increase);
        decrease=view.findViewById(R.id.decrease);
        comment=view.findViewById(R.id.comment);
        btnBook=view.findViewById(R.id.btn_book);
        date=view.findViewById(R.id.date);
        hour=view.findViewById(R.id.hour);
        dateTxt=view.findViewById(R.id.date_txt);
        hourTxt=view.findViewById(R.id.hour_txt);
        bookingLayout=view.findViewById(R.id.booking_layout);
    }

    // calander action ( to choose the date of the booking)
    public void onClickDatePick(View view) {
        DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                dateTxt.setText(sdf.format(myCalendar.getTime()));
            }

        };
        new DatePickerDialog(context, datePicker, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    // clock action ( to choose the time of the booking)
    public void onClickTimePick(View view) {
        TimePickerDialog.OnTimeSetListener timePicker = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                String myFormat = "HH:mm"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                hourTxt.setText(sdf.format(myCalendar.getTime()));
            }


        };
        new TimePickerDialog(context, timePicker, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true).show();
    }

}
