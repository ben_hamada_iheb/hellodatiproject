package com.android.hellodatiproject.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_ServiceFragment extends Fragment {

    public View view;
    public static Context context;
    public static SwipeRefreshLayout swipeContainer;
    public static ImageView hotelImg,hotelLogo;
    public static TextView hotelTitle;
    public static RatingBar hotelStars;
    public static RecyclerView recyclerView;
    public static FrameLayout frameLayout;

    public static String selectedService;

    public static State stateDAO=new State();
    public UI_ServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        frameLayout.removeAllViews();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                stateDAO.getAll("fr","iServiceFragment.getHotelData","iServiceFragment.getHotelDataError");
                stateDAO.getAll("fr","iServiceFragment.getServiceData","iServiceFragment.getServiceDataError");
                swipeContainer.setRefreshing(false);
            }
        });
        stateDAO.getAll("fr","iServiceFragment.getHotelData","iServiceFragment.getHotelDataError");
        stateDAO.getAll("fr","iServiceFragment.getServiceData","iServiceFragment.getServiceDataError");

        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui_service, container, false);
        swipeContainer=view.findViewById(R.id.swipe_container);
        hotelImg=view.findViewById(R.id.hotel_image);
        hotelLogo=view.findViewById(R.id.hotel_logo);
        hotelTitle=view.findViewById(R.id.hotel_title);
        hotelStars=view.findViewById(R.id.hotel_stars);
        recyclerView=view.findViewById(R.id.recycler_view);
        frameLayout=view.findViewById(R.id.choosen_content_layout);
    }

}
