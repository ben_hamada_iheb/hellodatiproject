package com.android.hellodatiproject.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.hellodatiproject.Adapter.CallOptionAdapter;
import com.android.hellodatiproject.R;

import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_CallFragment extends Fragment {

    public View view;
    public static RelativeLayout listOption;
    public static FrameLayout detailed;
    public static Context context;
    public static RecyclerView recyclerView;
    public List<CallOption> contents;
    public static CallOption selectedOption;

    public UI_CallFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        contents = Arrays.asList(
                new CallOption(CallOption.CallOptionCode.ROOM, context.getString(R.string.option_call_room_title), R.drawable.ic_chambre, context.getString(R.string.option_call_room_desc), context.getString(R.string.option_call_room_desc_long), context.getResources().getColor(R.color.dark_blue)),
                new CallOption(CallOption.CallOptionCode.EMERGENCY, context.getString(R.string.option_call_urgent_title), R.drawable.ic_urgence, context.getString(R.string.option_call_urgent_desc), context.getString(R.string.option_call_urgent_desc_long), context.getResources().getColor(R.color.shady_blue)),
                new CallOption(CallOption.CallOptionCode.ROOM_SERVICE, context.getString(R.string.option_call_room_service_title), R.drawable.ic_service_chambre, context.getString(R.string.option_call_room_service_desc), context.getString(R.string.option_call_room_service_desc_long), context.getResources().getColor(R.color.half_dark_blue)),
                new CallOption(CallOption.CallOptionCode.NATIONAL, context.getString(R.string.option_call_local_title), R.drawable.ic_local, context.getString(R.string.option_call_local_desc), context.getString(R.string.option_call_local_desc_long), context.getResources().getColor(R.color.half_light_blue)),
                new CallOption(CallOption.CallOptionCode.INTERNATIONAL, context.getString(R.string.option_call_international_title), R.drawable.ic_international, context.getString(R.string.option_call_international_desc), context.getString(R.string.option_call_international_desc_long), context.getResources().getColor(R.color.light_blue))
        );
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        CallOptionAdapter adapter = new CallOptionAdapter(contents, context);
        recyclerView.setAdapter(adapter);
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__call, container, false);
        recyclerView=view.findViewById(R.id.recycler_view);
        detailed=view.findViewById(R.id.detailed);
        listOption=view.findViewById(R.id.list_options);
    }

    // set the data in the content place
    public static void setFragmentOfTab(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
            ft.replace(detailed.getId(), fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }

}
