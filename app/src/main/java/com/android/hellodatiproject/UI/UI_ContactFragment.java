package com.android.hellodatiproject.UI;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_ContactFragment extends Fragment {

    public View view;
    public static Context context;
    public  static ImageButton btnBack;
    public static RatingBar hotelStars;
    public static LinearLayout mapLayout,hotelCallLayout;
    public static CircleImageView hotelLogo;
    public static ImageView hotelFaceBook,hotelTwitter,hotelYoutube;
    public static TextView serviceTitle,hotelTitle,hotelAddress,hotelTel,hotelMail;
    public static Button btnContactUs;

    public static State stateDAO=new State();


    public UI_ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        serviceTitle.setText(UI_ServiceFragment.selectedService);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_ServiceFragment.frameLayout.removeAllViews();
            }
        });
        hotelCallLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent phoneCall= context.getPackageManager().getLaunchIntentForPackage("com.datiphone");
                if(phoneCall!=null){
                    //send app lang
                    phoneCall.putExtra("lang", LocaleHelper.getLanguage(context));
                    phoneCall.putExtra("number", "70975424");
                    phoneCall.putExtra("limit", MainActivity.call_limit);
                    phoneCall.putExtra("call_time",  MainActivity.call_time);
                    //Log.d("call_time :", call_time+"");
                    //phoneCall.putExtra("list_number",  list_number);
                    phoneCall.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(phoneCall);
                }
                else {
                    Toast.makeText(context,context.getString(R.string.error_datiphone),Toast.LENGTH_LONG).show();
                }
            }
        });
        hotelFaceBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/CarthageThalassoResortGammarth/");
                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
        hotelTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://twitter.com/carthage_resort?lang=en");
                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
        hotelYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.youtube.com/channel/UCSdg0mlV1rQ_YU-I7rniK1A");
                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });
        mapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q="+"4 Rue Ibn Ennadim Tunis 1073"));
                context.startActivity(intent);
            }
        });
        btnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("azerty","sfsdfsdf");
                final Dialog myDialog = new Dialog(UI_ContactFragment.context);
                myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                final View listItem = LayoutInflater.from(UI_ContactFragment.context).inflate(R.layout.popup_contact_us, null, false);
                myDialog.setContentView(listItem);
                final EditText mailSender = listItem.findViewById(R.id.edit_email);
                final EditText mail = listItem.findViewById(R.id.editText_Email);
                final EditText subject = listItem.findViewById(R.id.subject);

                Button btn_send = listItem.findViewById(R.id.btn_sendContact);
                ImageView btn_cancel = listItem.findViewById(R.id.btn_cancelContact);
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });


                btn_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //send mail to the hotel by using API
                        myDialog.dismiss();
                    }
                });
                myDialog.setCancelable(false);
                myDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                myDialog.show();
            }
        });
        stateDAO.getAll("fr","iContact.getContactData","iContact.getContactDataError");
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view= inflater.inflate(R.layout.fragment_ui__contact, container, false);
        serviceTitle=view.findViewById(R.id.service_title);
        hotelStars=view.findViewById(R.id.hotel_stars);
        hotelFaceBook=view.findViewById(R.id.hotel_facebook);
        hotelTwitter=view.findViewById(R.id.hotel_twitter);
        hotelYoutube=view.findViewById(R.id.hotel_youtube);
        btnBack=view.findViewById(R.id.btn_back);
        hotelLogo=view.findViewById(R.id.hotel_logo);
        hotelTitle=view.findViewById(R.id.hotel_name);
        hotelAddress=view.findViewById(R.id.hotel_address);
        hotelTel=view.findViewById(R.id.hotel_tel);
        hotelMail=view.findViewById(R.id.hotel_mail);
        btnContactUs=view.findViewById(R.id.btn_contact_us);
        mapLayout=view.findViewById(R.id.map_layout);
        hotelCallLayout=view.findViewById(R.id.hotel_call_layout);
    }

}
