package com.android.hellodatiproject.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UI_LeisureListFragment extends Fragment {

    public View view;
    public static Context context;
    public static RecyclerView recyclerLeisureActivites,recyclerLeisureOffers;

    public static State stateDAO=new State();

    public UI_LeisureListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        initContent(inflater,container,savedInstanceState);
        UI_LeisureFragment.position=0;
        UI_LeisureFragment.tabs.setVisibility(View.GONE);
        UI_LeisureFragment.buttomHeader.setVisibility(View.GONE);
        stateDAO.getAll("fr","iLeisureAdapter.getLeisureActivitiesListData","iLeisureAdapter.getLeisureActivitiesListDataError");
        stateDAO.getAll("fr","iLeisureAdapter.getLeisureOffersListData","iLeisureAdapter.getLeisureOffersListDataError");
        return view;
    }

    private void initContent(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=getContext();
        view=inflater.inflate(R.layout.fragment_ui__leisure_list, container, false);
        recyclerLeisureActivites=view.findViewById(R.id.recycler_leisure_activites);
        recyclerLeisureOffers=view.findViewById(R.id.recycler_leisure_offers);
    }

}
