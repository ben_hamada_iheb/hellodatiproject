package com.android.hellodatiproject;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;


public class HelloDatiApplication extends Application {

    public static HelloDatiApplication instance;
    private static final int REQUESTING_CALL_PERMISSION = 1002;
    public static final int REQUESTING_PHONE_STATE_PERMISSION = 1001;
    static boolean kioskModeOn = false;

    public static boolean isKioskModeOn() {
        return kioskModeOn;
    }

    public static void setKioskModeOn(boolean kioskModeOn) {
        HelloDatiApplication.kioskModeOn = kioskModeOn;
    }

    public static String[] getAllowPackages() {
        return new String[]{
                "com.hellodati.datiapp",
                // Torch
                "com.example.torch",
                // Fm Radio
                "com.android.fmradio",
                // Oukitel Camera
                "com.mediatek.camera",
                // Google SearchBox
                "com.google.android.googlequicksearchbox",
                // YouTube
                "com.google.android.youtube",
                // Google Music
                "com.google.android.music",
                // Google Contacts
                "com.google.android.contacts",
                // Google Calendar
                "com.google.android.calendar",
                // Google Maps
                "com.google.android.apps.maps",
                // Google Photos
                "com.google.android.apps.photos",
                "com.google.android.apps.docs",
                // Chrome
                "com.android.chrome",
                //Face Unlock
                //"com.elephanttek.faceunlock",
                // Settings
                "com.android.settings",
                // Sound Recorder
                "com.android.soundrecorder",
                // Calculator
                "com.android.calculator2",
                // Battery Warning
                "com.mediatek.batterywarning",
                // Simple Weather
                "com.example.martinruiz.myapplication",
                // Simple Prayer
                "com.example.sun",
                // Simple Chat
                "com.example.chatapp_c13",
                // Simple PhoneCall
                "com.datiphone",
                // LockScreen app
                "com.eagle.locker",

                /*"com.mediatek.gba",
                "com.mediatek.ims",
                "com.android.cts.priv.ctsshim",
                "com.android.internal.display.cutout.emulation.corner",
                "com.google.android.ext.services",
                "com.android.internal.display.cutout.emulation.double",
                //"com.android.providers.telephony",
                "com.adups.fota.sysoper",
                "com.google.android.googlequicksearchbox",
                "com.android.providers.media",
                "com.google.android.onetimeinitializer",
                "com.google.android.ext.shared",
                "com.mediatek.location.lppe.main",
                "com.android.wallpapercropper",
                "com.android.protips",
                "com.android.documentsui",
                "android.auto_generated_rro__",
                "com.android.externalstorage",
                "com.mediatek.ygps",
                "com.mediatek.simprocessor",
                "com.android.htmlviewer",
                "com.mediatek.mms.appservice",
                "com.android.companiondevicemanager",
                "com.android.mms.service",
                "com.android.providers.downloads",
                "com.google.android.apps.messaging",
                "com.adups.fota",
                "com.mediatek.engineermode",
                "com.android.partnerbrowsercustomizations.example",
                "com.mediatek.omacp",
                "com.google.android.configupdater",
                "com.mediatek.wfo.impl",
                "com.android.defcontainer",
                "com.android.providers.downloads.ui",
                "com.android.vending",
                "com.android.pacprocessor",
                "com.android.simappdialog",
                "com.android.internal.display.cutout.emulation.tall",
                "com.android.certinstaller",
                "com.android.carrierconfig",
                "com.google.android.marvin.talkback",
                "com.android.egg",
                "com.android.mtp",
                "com.android.stk",
                "com.android.launcher3",
                "com.android.backupconfirm",
                "com.android.statementservice",
                "com.google.android.gm",
                "com.google.android.apps.tachyon",
                "com.mediatek.mdmlsample",
                "com.android.settings.intelligence",
                "com.mediatek.providers.drm",
                "com.android.systemui.theme.dark",
                "com.wtk.factory",
                "com.google.android.setupwizard",
                "com.android.sharedstoragebackup",
                "com.mediatek.batterywarning",
                "com.android.printspooler",
                "com.android.dreams.basic",
                "com.android.se",
                "com.android.inputdevices",
                "com.android.bips",
                "com.mediatek",
                "com.google.android.apps.nbu.files",
                "com.example",
                "com.mediatek.duraspeed",
                "com.android.musicfx",
                "com.google.android.webview",
                "ma.android.com.mafactory",
                "com.mediatek.nlpservice",
                "com.android.server.telecom",
                "com.google.android.syncadapters.contacts",
                "com.android.keychain",
                "com.wtk.stresstest",
                //"com.android.dialer",
                "com.google.android.packageinstaller",
                "com.google.android.gms",
                "com.google.android.gsf",
                "com.google.android.ims",
                "com.google.android.tts",
                "com.android.calllogbackup",
                "com.google.android.partnersetup",
                "com.google.android.videos",
                "com.android.proxyhandler",
                "com.google.android.feedback",
                "com.google.android.printservice.recommendation",
                "com.android.managedprovisioning",
                "com.mediatek.thermalmanager",
                "com.mediatek.callrecorder",
                "com.android.providers.partnerbookmarks",
                "com.android.smspush",
                "com.android.wallpaper.livepicker",
                "com.google.android.gms.policy_sidecar_aps",
                "com.baidu.map.location",
                "com.google.android.backuptransport",
                "com.android.bookmarkprovider",
                "com.mediatek.mdmconfig",
                "com.mediatek.lbs.em2.ui",
                "com.android.cts.ctsshim",
                "com.android.vpndialogs",
                "com.android.shell",
                "com.android.wallpaperbackup",
                "com.android.providers.blockednumber",
                "com.android.providers.userdictionary",
                "com.android.emergency",
                "com.android.location.fused",
                "com.android.deskclock",
                "com.android.systemui",
                "com.android.bluetoothmidiservice",
                "com.mediatek.location.mtknlp",
                "com.android.traceur",
                "com.mediatek.mtklogger",
                "com.android.bluetooth",
                "com.android.wallpaperpicker",
                "com.android.providers.contacts",
                "com.android.captiveportallogin",
                "com.mediatek.dataprotection",
                "com.google.android.inputmethod.latin",
                "com.google.android.apps.restore",*/
        };
    }

    public static String[] getAllowPackagesExipred() {
        return new String[]{
                "com.hellodati.datiapp",
                // Torch
                "com.example.torch",
                // Fm Radio
                "com.android.fmradio",
                // Oukitel Camera
                "com.mediatek.camera",
                // Google SearchBox
                //"com.google.android.googlequicksearchbox",
                // YouTube
                //"com.google.android.youtube",
                // Google Music
                //"com.google.android.music",
                // Google Contacts
                "com.google.android.contacts",
                // Google Calendar
                "com.google.android.calendar",
                // Google Maps
                "com.google.android.apps.maps",
                // Google Photos
                //"com.google.android.apps.photos",
                // Google Drive
                //"com.google.android.apps.docs",
                // Chrome
                //"com.android.chrome",
                //Face Unlock
                //"com.elephanttek.faceunlock",
                // Settings
                "com.android.settings",
                // Sound Recorder
                "com.android.soundrecorder",
                // Calculator
                "com.android.calculator2",
                // Battery Warning
                "com.mediatek.batterywarning",
                // Simple Weather
                "com.example.martinruiz.myapplication",
                // Simple Prayer
                "com.example.sun",
                // Simple Chat
                "com.example.chatapp_c13",
                // Simple PhoneCall
                "com.datiphone",
                // LockScreen app
                "com.eagle.locker",

                /*"com.mediatek.gba",
                "com.mediatek.ims",
                "com.android.cts.priv.ctsshim",
                "com.android.internal.display.cutout.emulation.corner",
                "com.google.android.ext.services",
                "com.android.internal.display.cutout.emulation.double",
                //"com.android.providers.telephony",
                "com.adups.fota.sysoper",
                "com.google.android.googlequicksearchbox",
                "com.android.providers.media",
                "com.google.android.onetimeinitializer",
                "com.google.android.ext.shared",
                "com.mediatek.location.lppe.main",
                "com.android.wallpapercropper",
                "com.android.protips",
                "com.android.documentsui",
                "android.auto_generated_rro__",
                "com.android.externalstorage",
                "com.mediatek.ygps",
                "com.mediatek.simprocessor",
                "com.android.htmlviewer",
                "com.mediatek.mms.appservice",
                "com.android.companiondevicemanager",
                "com.android.mms.service",
                "com.android.providers.downloads",
                "com.google.android.apps.messaging",
                "com.adups.fota",
                "com.mediatek.engineermode",
                "com.android.partnerbrowsercustomizations.example",
                "com.mediatek.omacp",
                "com.google.android.configupdater",
                "com.mediatek.wfo.impl",
                "com.android.defcontainer",
                "com.android.providers.downloads.ui",
                "com.android.vending",
                "com.android.pacprocessor",
                "com.android.simappdialog",
                "com.android.internal.display.cutout.emulation.tall",
                "com.android.certinstaller",
                "com.android.carrierconfig",
                "com.google.android.marvin.talkback",
                "com.android.egg",
                "com.android.mtp",
                "com.android.stk",
                "com.android.launcher3",
                "com.android.backupconfirm",
                "com.android.statementservice",
                "com.google.android.gm",
                "com.google.android.apps.tachyon",
                "com.mediatek.mdmlsample",
                "com.android.settings.intelligence",
                "com.mediatek.providers.drm",
                "com.android.systemui.theme.dark",
                "com.wtk.factory",
                "com.google.android.setupwizard",
                "com.android.sharedstoragebackup",
                "com.mediatek.batterywarning",
                "com.android.printspooler",
                "com.android.dreams.basic",
                "com.android.se",
                "com.android.inputdevices",
                "com.android.bips",
                "com.mediatek",
                "com.google.android.apps.nbu.files",
                "com.example",
                "com.mediatek.duraspeed",
                "com.android.musicfx",
                "com.google.android.webview",
                "ma.android.com.mafactory",
                "com.mediatek.nlpservice",
                "com.android.server.telecom",
                "com.google.android.syncadapters.contacts",
                "com.android.keychain",
                "com.wtk.stresstest",
                //"com.android.dialer",
                "com.google.android.packageinstaller",
                "com.google.android.gms",
                "com.google.android.gsf",
                "com.google.android.ims",
                "com.google.android.tts",
                "com.android.calllogbackup",
                "com.google.android.partnersetup",
                "com.google.android.videos",
                "com.android.proxyhandler",
                "com.google.android.feedback",
                "com.google.android.printservice.recommendation",
                "com.android.managedprovisioning",
                "com.mediatek.thermalmanager",
                "com.mediatek.callrecorder",
                "com.android.providers.partnerbookmarks",
                "com.android.smspush",
                "com.android.wallpaper.livepicker",
                "com.google.android.gms.policy_sidecar_aps",
                "com.baidu.map.location",
                "com.google.android.backuptransport",
                "com.android.bookmarkprovider",
                "com.mediatek.mdmconfig",
                "com.mediatek.lbs.em2.ui",
                "com.android.cts.ctsshim",
                "com.android.vpndialogs",
                "com.android.shell",
                "com.android.wallpaperbackup",
                "com.android.providers.blockednumber",
                "com.android.providers.userdictionary",
                "com.android.emergency",
                "com.android.location.fused",
                "com.android.deskclock",
                "com.android.systemui",
                "com.android.bluetoothmidiservice",
                "com.mediatek.location.mtknlp",
                "com.android.traceur",
                "com.mediatek.mtklogger",
                "com.android.bluetooth",
                "com.android.wallpaperpicker",
                "com.android.providers.contacts",
                "com.android.captiveportallogin",
                "com.mediatek.dataprotection",
                "com.google.android.inputmethod.latin",
                "com.google.android.apps.restore",*/
        };
    }

    public static String[] getAllowPackagesForShow() {
        return new String[]{
                "com.hellodati.datiapp",
                // Torch
                "com.example.torch",
                // Fm Radio
                "com.android.fmradio",
                // Oukitel Camera
                "com.mediatek.camera",
                // Google SearchBox
                "com.google.android.googlequicksearchbox",
                // YouTube
                "com.google.android.youtube",
                // Google Music
                "com.google.android.music",
                // Google Contacts
                "com.google.android.contacts",
                // Google Calendar
                "com.google.android.calendar",
                // Google Maps
                "com.google.android.apps.maps",
                // Google Photos
                "com.google.android.apps.photos",
                // Google Drive
                "com.google.android.apps.docs",
                // Chrome
                "com.android.chrome",
                //Face Unlock
                //"com.elephanttek.faceunlock",
                // Settings
                "com.android.settings",
                // Sound Recorder
                "com.android.soundrecorder",
                // Calculator
                "com.android.calculator2",
                // Battery Warning
                "com.mediatek.batterywarning",
                // Simple Weather
                "com.example.martinruiz.myapplication",
                // Simple Prayer
                "com.example.sun",
                // Simple Chat
                "com.example.chatapp_c13",
                // Simple PhoneCall
                //"com.datiphone",
                // LockScreen app
                //"com.eagle.locker",

                /*"com.mediatek.gba",
                "com.mediatek.ims",
                "com.android.cts.priv.ctsshim",
                "com.android.internal.display.cutout.emulation.corner",
                "com.google.android.ext.services",
                "com.android.internal.display.cutout.emulation.double",
                "com.android.providers.telephony",
                "com.adups.fota.sysoper",
                "com.google.android.googlequicksearchbox",
                "com.android.providers.media",
                "com.google.android.onetimeinitializer",
                "com.google.android.ext.shared",
                "com.mediatek.location.lppe.main",
                "com.android.wallpapercropper",
                "com.android.protips",
                "com.android.documentsui",
                "android.auto_generated_rro__",
                "com.android.externalstorage",
                "com.mediatek.ygps",
                "com.mediatek.simprocessor",
                "com.android.htmlviewer",
                "com.mediatek.mms.appservice",
                "com.android.companiondevicemanager",
                "com.android.mms.service",
                "com.android.providers.downloads",
                "com.google.android.apps.messaging",
                "com.adups.fota",
                "com.mediatek.engineermode",
                "com.android.partnerbrowsercustomizations.example",
                "com.mediatek.omacp",
                "com.google.android.configupdater",
                "com.mediatek.wfo.impl",
                "com.android.defcontainer",
                "com.android.providers.downloads.ui",
                "com.android.vending",
                "com.android.pacprocessor",
                "com.android.simappdialog",
                "com.android.internal.display.cutout.emulation.tall",
                "com.android.certinstaller",
                "com.android.carrierconfig",
                "com.google.android.marvin.talkback",
                "com.android.egg",
                "com.android.mtp",
                "com.android.stk",
                "com.android.launcher3",
                "com.android.backupconfirm",
                "com.android.statementservice",
                "com.google.android.gm",
                "com.google.android.apps.tachyon",
                "com.mediatek.mdmlsample",
                "com.android.settings.intelligence",
                "com.mediatek.providers.drm",
                "com.android.systemui.theme.dark",
                "com.wtk.factory",
                "com.google.android.setupwizard",
                "com.android.sharedstoragebackup",
                "com.mediatek.batterywarning",
                "com.android.printspooler",
                "com.android.dreams.basic",
                "com.android.se",
                "com.android.inputdevices",
                "com.android.bips",
                "com.mediatek",
                "com.google.android.apps.nbu.files",
                "com.example",
                "com.mediatek.duraspeed",
                "com.android.musicfx",
                "com.google.android.webview",
                "ma.android.com.mafactory",
                "com.mediatek.nlpservice",
                "com.android.server.telecom",
                "com.google.android.syncadapters.contacts",
                "com.android.keychain",
                "com.wtk.stresstest",
                "com.android.dialer",
                "com.google.android.packageinstaller",
                "com.google.android.gms",
                "com.google.android.gsf",
                "com.google.android.ims",
                "com.google.android.tts",
                "com.android.calllogbackup",
                "com.google.android.partnersetup",
                "com.google.android.videos",
                "com.android.proxyhandler",
                "com.google.android.feedback",
                "com.google.android.printservice.recommendation",
                "com.android.managedprovisioning",
                "com.mediatek.thermalmanager",
                "com.mediatek.callrecorder",
                "com.android.providers.partnerbookmarks",
                "com.android.smspush",
                "com.android.wallpaper.livepicker",
                "com.google.android.gms.policy_sidecar_aps",
                "com.baidu.map.location",
                "com.google.android.backuptransport",
                "com.android.bookmarkprovider",
                "com.mediatek.mdmconfig",
                "com.mediatek.lbs.em2.ui",
                "com.android.cts.ctsshim",
                "com.android.vpndialogs",
                "com.android.shell",
                "com.android.wallpaperbackup",
                "com.android.providers.blockednumber",
                "com.android.providers.userdictionary",
                "com.android.emergency",
                "com.android.location.fused",
                "com.android.deskclock",
                "com.android.systemui",
                "com.android.bluetoothmidiservice",
                "com.mediatek.location.mtknlp",
                "com.android.traceur",
                "com.mediatek.mtklogger",
                "com.android.bluetooth",
                "com.android.wallpaperpicker",
                "com.android.providers.contacts",
                "com.android.captiveportallogin",
                "com.mediatek.dataprotection",
                "com.google.android.inputmethod.latin",
                "com.google.android.apps.restore",*/
        };
    }

    public static String[] getAllowPackagesForShowWhenExpiredPackage() {
        return new String[]{
                "com.hellodati.datiapp",
                // Torch
                "com.example.torch",
                // Fm Radio
                "com.android.fmradio",
                // Oukitel Camera
                "com.mediatek.camera",
                // Google SearchBox
                //"com.google.android.googlequicksearchbox",
                // YouTube
                //"com.google.android.youtube",
                // Google Music
                "com.google.android.music",
                // Google Contacts
                "com.google.android.contacts",
                // Google Calendar
                "com.google.android.calendar",
                // Google Maps
                "com.google.android.apps.maps",
                // Google Photos
                //"com.google.android.apps.photos",
                // Google Drive
                //"com.google.android.apps.docs",
                // Chrome
                //"com.android.chrome",
                //Face Unlock
                //"com.elephanttek.faceunlock",
                // Settings
                "com.android.settings",
                // Sound Recorder
                "com.android.soundrecorder",
                // Calculator
                "com.android.calculator2",
                // Battery Warning
                "com.mediatek.batterywarning",
                // Simple Weather
                "com.example.martinruiz.myapplication",
                // Simple Prayer
                "com.example.sun",
                // Simple Chat
                "com.example.chatapp_c13",
                // Simple PhoneCall
                //"com.datiphone",
                // LockScreen app
                //"com.eagle.locker",

                /*"com.mediatek.gba",
                "com.mediatek.ims",
                "com.android.cts.priv.ctsshim",
                "com.android.internal.display.cutout.emulation.corner",
                "com.google.android.ext.services",
                "com.android.internal.display.cutout.emulation.double",
                "com.android.providers.telephony",
                "com.adups.fota.sysoper",
                "com.google.android.googlequicksearchbox",
                "com.android.providers.media",
                "com.google.android.onetimeinitializer",
                "com.google.android.ext.shared",
                "com.mediatek.location.lppe.main",
                "com.android.wallpapercropper",
                "com.android.protips",
                "com.android.documentsui",
                "android.auto_generated_rro__",
                "com.android.externalstorage",
                "com.mediatek.ygps",
                "com.mediatek.simprocessor",
                "com.android.htmlviewer",
                "com.mediatek.mms.appservice",
                "com.android.companiondevicemanager",
                "com.android.mms.service",
                "com.android.providers.downloads",
                "com.google.android.apps.messaging",
                "com.adups.fota",
                "com.mediatek.engineermode",
                "com.android.partnerbrowsercustomizations.example",
                "com.mediatek.omacp",
                "com.google.android.configupdater",
                "com.mediatek.wfo.impl",
                "com.android.defcontainer",
                "com.android.providers.downloads.ui",
                "com.android.vending",
                "com.android.pacprocessor",
                "com.android.simappdialog",
                "com.android.internal.display.cutout.emulation.tall",
                "com.android.certinstaller",
                "com.android.carrierconfig",
                "com.google.android.marvin.talkback",
                "com.android.egg",
                "com.android.mtp",
                "com.android.stk",
                "com.android.launcher3",
                "com.android.backupconfirm",
                "com.android.statementservice",
                "com.google.android.gm",
                "com.google.android.apps.tachyon",
                "com.mediatek.mdmlsample",
                "com.android.settings.intelligence",
                "com.mediatek.providers.drm",
                "com.android.systemui.theme.dark",
                "com.wtk.factory",
                "com.google.android.setupwizard",
                "com.android.sharedstoragebackup",
                "com.mediatek.batterywarning",
                "com.android.printspooler",
                "com.android.dreams.basic",
                "com.android.se",
                "com.android.inputdevices",
                "com.android.bips",
                "com.mediatek",
                "com.google.android.apps.nbu.files",
                "com.example",
                "com.mediatek.duraspeed",
                "com.android.musicfx",
                "com.google.android.webview",
                "ma.android.com.mafactory",
                "com.mediatek.nlpservice",
                "com.android.server.telecom",
                "com.google.android.syncadapters.contacts",
                "com.android.keychain",
                "com.wtk.stresstest",
                "com.android.dialer",
                "com.google.android.packageinstaller",
                "com.google.android.gms",
                "com.google.android.gsf",
                "com.google.android.ims",
                "com.google.android.tts",
                "com.android.calllogbackup",
                "com.google.android.partnersetup",
                "com.google.android.videos",
                "com.android.proxyhandler",
                "com.google.android.feedback",
                "com.google.android.printservice.recommendation",
                "com.android.managedprovisioning",
                "com.mediatek.thermalmanager",
                "com.mediatek.callrecorder",
                "com.android.providers.partnerbookmarks",
                "com.android.smspush",
                "com.android.wallpaper.livepicker",
                "com.google.android.gms.policy_sidecar_aps",
                "com.baidu.map.location",
                "com.google.android.backuptransport",
                "com.android.bookmarkprovider",
                "com.mediatek.mdmconfig",
                "com.mediatek.lbs.em2.ui",
                "com.android.cts.ctsshim",
                "com.android.vpndialogs",
                "com.android.shell",
                "com.android.wallpaperbackup",
                "com.android.providers.blockednumber",
                "com.android.providers.userdictionary",
                "com.android.emergency",
                "com.android.location.fused",
                "com.android.deskclock",
                "com.android.systemui",
                "com.android.bluetoothmidiservice",
                "com.mediatek.location.mtknlp",
                "com.android.traceur",
                "com.mediatek.mtklogger",
                "com.android.bluetooth",
                "com.android.wallpaperpicker",
                "com.android.providers.contacts",
                "com.android.captiveportallogin",
                "com.mediatek.dataprotection",
                "com.google.android.inputmethod.latin",
                "com.google.android.apps.restore",*/
        };
    }

    public static boolean checkPhoneStatePermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(instance, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUESTING_PHONE_STATE_PERMISSION);
            }
            return false;
        }
        return true;
    }

    public static String getImei(Activity activity) {
        TelephonyManager telephonyManager = (TelephonyManager) instance.getSystemService(instance.TELEPHONY_SERVICE);
        if (!checkPhoneStatePermission(activity)) {
            return "empty";
        } else {
            return telephonyManager.getDeviceId();
        }
    }
}
