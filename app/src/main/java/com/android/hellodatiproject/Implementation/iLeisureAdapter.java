package com.android.hellodatiproject.Implementation;

import android.support.v7.widget.LinearLayoutManager;

import com.android.hellodatiproject.Adapter.LeisureActivitiesAdapter;
import com.android.hellodatiproject.Adapter.LeisureOffersAdapter;
import com.android.hellodatiproject.UI.UI_LeisureListFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iLeisureAdapter {

    public static void getLeisureActivitiesListData(JSONArray data) throws JSONException {
        UI_LeisureListFragment.recyclerLeisureActivites.setLayoutManager(new LinearLayoutManager(UI_LeisureListFragment.context));
        LeisureActivitiesAdapter adapter = new LeisureActivitiesAdapter(data, UI_LeisureListFragment.context);
        UI_LeisureListFragment.recyclerLeisureActivites.setAdapter(adapter);
    }

    public static void getLeisureActivitiesListDataError(int errorCode, String errorMSG){

    }

    public static void getLeisureOffersListData(JSONArray data) throws JSONException {
        LinearLayoutManager lm = new LinearLayoutManager(UI_LeisureListFragment.context);
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        UI_LeisureListFragment.recyclerLeisureOffers.setLayoutManager(lm);
        LeisureOffersAdapter adapter = new LeisureOffersAdapter(data, UI_LeisureListFragment.context);
        UI_LeisureListFragment.recyclerLeisureOffers.setAdapter(adapter);
    }

    public static void getLeisureOffersListDataError(int errorCode, String errorMSG){

    }

}
