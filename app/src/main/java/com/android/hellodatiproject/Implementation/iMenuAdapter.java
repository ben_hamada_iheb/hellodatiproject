package com.android.hellodatiproject.Implementation;

import android.support.v7.widget.LinearLayoutManager;

import com.android.hellodatiproject.Adapter.CategoriesMenuAdapter;
import com.android.hellodatiproject.Adapter.ContentMenuAdapter;
import com.android.hellodatiproject.UI.UI_MenuFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class iMenuAdapter {

    public static void getCategoriesData(JSONArray data) throws JSONException {
        LinearLayoutManager lm = new LinearLayoutManager(UI_MenuFragment.context);
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        UI_MenuFragment.categoriesRecycler.setLayoutManager(lm);
        CategoriesMenuAdapter adapter = new CategoriesMenuAdapter(data, UI_MenuFragment.context);
        UI_MenuFragment.categoriesRecycler.setAdapter(adapter);
    }

    public static void getCategoriesDataError(int errorCode, String errorMSG){

    }

    public static void getContentData(JSONArray data) throws JSONException {
        UI_MenuFragment.contentRecycler.setLayoutManager(new LinearLayoutManager(UI_MenuFragment.context));
        ContentMenuAdapter adapter = new ContentMenuAdapter(data, UI_MenuFragment.context);
        UI_MenuFragment.contentRecycler.setAdapter(adapter);
    }

    public static void getContentDataError(int errorCode, String errorMSG){

    }

}
