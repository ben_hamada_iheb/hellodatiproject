package com.android.hellodatiproject.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.android.hellodatiproject.Adapter.NotificationsAdapter;
import com.android.hellodatiproject.UI.Notification;

import org.json.JSONArray;
import org.json.JSONException;

public class iNotification {
    public static void getNotificationListData(JSONArray data) throws JSONException {
        //data=null;
        if(data==null){
            Notification.emptyNotification.setVisibility(View.VISIBLE);
            Notification.recyclerView.setVisibility(View.GONE);
        }
        else {
            Notification.emptyNotification.setVisibility(View.GONE);
            Notification.recyclerView.setVisibility(View.VISIBLE);
            Notification.recyclerView.setLayoutManager(new LinearLayoutManager(Notification.context));
            NotificationsAdapter adapter = new NotificationsAdapter(data, Notification.context);
            Notification.recyclerView.setAdapter(adapter);
        }
    }

    public static void getNotificationListDataError(int errorCode, String errorMSG){

    }
}
