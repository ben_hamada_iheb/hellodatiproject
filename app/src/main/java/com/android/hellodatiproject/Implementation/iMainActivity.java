package com.android.hellodatiproject.Implementation;

import android.util.Log;

import com.android.hellodatiproject.UI.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

public class iMainActivity {
    public static void getNotificationData(JSONArray data) throws JSONException {
        Log.d("MainActivity : get notification data onSuccess -> ",data.getJSONObject(0).toString());
        MainActivity.notificationData=null;
    }

    public static void getNotificationDataError(int errorCode, String errorMSG){
        Log.e("MainActivity : get notification data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }
}
