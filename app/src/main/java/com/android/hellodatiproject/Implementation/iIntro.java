package com.android.hellodatiproject.Implementation;

import android.util.Log;

import com.android.hellodatiproject.UI.UI_IntroActivity;

import org.json.JSONArray;
import org.json.JSONException;

public class iIntro {

    public static void getData(JSONArray data){
        try {
            Log.d("UI_IntroActivity : get data onSuccess -> ",data.getJSONObject(0).toString());
            UI_IntroActivity.hotelTitle.setText(data.getJSONObject(0).getString("name"));
            UI_IntroActivity.hotelStars.setRating(Integer.valueOf(data.getJSONObject(0).getString("id")));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getDataError(int errorCode, String errorMSG){
        Log.e("UI_IntroActivity : get data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

}
