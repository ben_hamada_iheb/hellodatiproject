package com.android.hellodatiproject.Implementation;

import android.util.Log;

import com.android.hellodatiproject.UI.UI_IntroWelcomeActivity;

import org.json.JSONArray;
import org.json.JSONException;

public class iIntroWelcome {

    public static void getData(JSONArray data){
        try {
            Log.d("IntroWelcomeActivity : get data onSuccess -> ",data.getJSONObject(0).toString());

            UI_IntroWelcomeActivity.hotelTitle.setText(data.getJSONObject(0).getString("name"));
            UI_IntroWelcomeActivity.hotelStars.setRating(Integer.valueOf(data.getJSONObject(0).getString("id")));
            UI_IntroWelcomeActivity.touristName.setText(data.getJSONObject(0).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getDataError(int errorCode, String errorMSG){
        Log.e("IntroWelcomeActivity : get data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }


}
