package com.android.hellodatiproject.Implementation;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.android.hellodatiproject.Adapter.HistoryAdapter;
import com.android.hellodatiproject.UI.HCommands;
import com.android.hellodatiproject.UI.HReservations;

import org.json.JSONArray;
import org.json.JSONException;

public class iHistoryFragment {

    public static void getCommandsHistoryData(final JSONArray data) throws JSONException {
        HCommands.recyclerViewHistory.setLayoutManager(new LinearLayoutManager(HCommands.context));
        HistoryAdapter adapter = new HistoryAdapter(data, HCommands.context);
        HCommands.recyclerViewHistory.setAdapter(adapter);
        HCommands.progressBar.setVisibility(View.GONE);

    }


    public static void getCommandsHistoryDataError(int errorCode, String errorMSG){
        Log.e("ServiceFragment : get commands history data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

    public static void getReservationsHistoryData(final JSONArray data) throws JSONException {
        HReservations.recyclerViewHistory.setLayoutManager(new LinearLayoutManager(HReservations.context));
        HistoryAdapter adapter = new HistoryAdapter(data, HReservations.context);
        HReservations.recyclerViewHistory.setAdapter(adapter);
        HReservations.progressBar.setVisibility(View.GONE);

    }


    public static void getReservationsHistoryDataError(int errorCode, String errorMSG){
        Log.e("ServiceFragment : get reservations history data onError -> ", "Code ("+errorCode+") * message("+errorMSG+")--------!!!!!!!!????????");
    }

}
