package com.android.hellodatiproject;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.hellodatiproject.UI.UI_IntroWelcomeActivity;
import com.android.hellodatiproject.update.DownloadUpdate;
import com.android.hellodatiproject.update.UpdateScheduler;

public class AdminReceiver extends DeviceAdminReceiver {

    @Override
    public void onEnabled(Context context, Intent intent) {
        //Toast.makeText(context, context.getString(R.string.device_admin_enabled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return null;
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        //Toast.makeText(context, context.getString(R.string.device_admin_disabled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLockTaskModeEntering(Context context, Intent intent, String pkg) {
        //Toast.makeText(context, context.getString(R.string.kiosk_mode_enabled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLockTaskModeExiting(Context context, Intent intent) {
        //Toast.makeText(context, context.getString(R.string.kiosk_mode_disabled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.i("AdminReceiver", "Received: " + intent.getAction());
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            context.startActivity(UI_IntroWelcomeActivity.RegularLaunch(context));
            new DownloadUpdate(context, null).execute();
            UpdateScheduler.scheduleJob(context);
        } else if ("android.intent.action.MY_PACKAGE_REPLACED".equals(intent.getAction())) {
            context.startActivity(UI_IntroWelcomeActivity.RegularLaunch(context));
        } else if ("com.madcatgames.roamfree.CHECK_UPDATE_ALARM".equals(intent.getAction())) {
            new DownloadUpdate(context, null).execute();
        }
    }
}