package com.android.hellodatiproject.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.hellodatiproject.DAO.State;
import com.android.hellodatiproject.R;
import com.android.hellodatiproject.UI.CallOption;
import com.android.hellodatiproject.UI.ContactListFragment;
import com.android.hellodatiproject.UI.LocaleHelper;
import com.android.hellodatiproject.UI.MainActivity;
import com.android.hellodatiproject.UI.UI_CallFragment;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class CallOptionAdapter extends RecyclerView.Adapter<CallOptionAdapter.ViewHolder>{
    List<CallOption> contents;
    Context context;
    public static State stateDAO=new State();



    public CallOptionAdapter(List<CallOption> contents, Context context){
        this.contents=contents;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.call_option, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.cardColor.setColorFilter(contents.get(i).mColor);
        viewHolder.cardIcon.setImageResource(contents.get(i).mIcon);
        viewHolder.cardTitle.setText(contents.get(i).mTitle);
        viewHolder.cardSummery.setText(contents.get(i).mSummery);
        viewHolder.cardInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog myDialogLangs = new Dialog(v.getContext());
                myDialogLangs.requestWindowFeature(Window.FEATURE_NO_TITLE);
                final View listItem = LayoutInflater.from(v.getContext()).inflate(R.layout.popup_info, null, false);
                myDialogLangs.setContentView(listItem);

                Button btn_ok = listItem.findViewById(R.id.btn_ok);
                ImageView icon = listItem.findViewById(R.id.icon_call);
                TextView txt_title = listItem.findViewById(R.id.title_call);
                TextView txt_description = listItem.findViewById(R.id.description_call);

                icon.setImageResource(contents.get(i).mIcon);
                txt_title.setText(contents.get(i).mTitle);
                txt_description.setText(contents.get(i).mDescription);


                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialogLangs.dismiss();
                    }
                });
                myDialogLangs.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                myDialogLangs.show();
            }
        });
        viewHolder.cardColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent phoneCall= context.getPackageManager().getLaunchIntentForPackage("com.datiphone");
                if(phoneCall!=null){
                    switch (i){
                        case 0:
                        case 1:
                        case 4:{
                            UI_CallFragment.selectedOption=contents.get(i);
                            stateDAO.getAll("fr","iCallContactAdapter.getContactData","iCallContactAdapter.getContactDataError");
                            showContacts();
                            break;
                        }
                        case 2:
                            //number list of intra flott
                        /*String[] list_number=new String[devicesResponse.getData().size()];
                        for (int i = 0; i < devicesResponse.getData().size(); i++) {
                            //Log.d("number", devicesResponse.getData().get(i).getNumber());
                            list_number[i]=devicesResponse.getData().get(i).getNumber();
                        }*/
                            //get call limit and call time from API or MainActivity
                        /*int limit = HelloDatiApplication.get().getDevice().getCall_limit();
                        int call_time = HelloDatiApplication.get().getDevice().getCall_time();*/
                            //Log.d("call_time :", call_time+"");
                            //Log.d("last_call_time :", MainActivity.lastCallTime+"");
                        /*if(call_time<MainActivity.lastCallTime)
                            call_time=MainActivity.lastCallTime;*/
                            //phoneCall.putExtra("limit",  limit);
                            //send app lang
                            phoneCall.putExtra("lang", LocaleHelper.getLanguage(context));
                            phoneCall.putExtra("number", "70975424");
                            phoneCall.putExtra("limit", MainActivity.call_limit);
                            phoneCall.putExtra("call_time",  MainActivity.call_time);
                            //Log.d("call_time :", call_time+"");
                            //phoneCall.putExtra("list_number",  list_number);
                            phoneCall.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(phoneCall);
                            break;
                        case 3:{
                            //number list of intra flott
                        /*String[] list_number=new String[devicesResponse.getData().size()];
                        for (int i = 0; i < devicesResponse.getData().size(); i++) {
                            //Log.d("number", devicesResponse.getData().get(i).getNumber());
                            list_number[i]=devicesResponse.getData().get(i).getNumber();
                        }*/
                            //get call limit and call time from API or MainActivity
                        /*int limit = HelloDatiApplication.get().getDevice().getCall_limit();
                        int call_time = HelloDatiApplication.get().getDevice().getCall_time();*/
                            //Log.d("call_time :", call_time+"");
                            //Log.d("last_call_time :", MainActivity.lastCallTime+"");
                        /*if(call_time<MainActivity.lastCallTime)
                            call_time=MainActivity.lastCallTime;*/
                            //phoneCall.putExtra("limit",  limit);
                            //send app lang
                            phoneCall.putExtra("lang", LocaleHelper.getLanguage(context));
                            phoneCall.putExtra("limit", MainActivity.call_limit);
                            phoneCall.putExtra("call_time",  MainActivity.call_time);
                            //Log.d("call_time :", call_time+"");
                            //phoneCall.putExtra("list_number",  list_number);
                            phoneCall.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(phoneCall);
                            break;
                        }
                    }
                }
                else {
                    Toast.makeText(context,context.getString(R.string.error_datiphone),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    void showContacts() {
        UI_CallFragment.detailed.removeAllViews();
        UI_CallFragment.listOption.setVisibility(View.GONE);
        UI_CallFragment.detailed.setVisibility(View.VISIBLE);
        UI_CallFragment.setFragmentOfTab(new ContactListFragment());
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cardColor,cardIcon,cardInformation;
        TextView cardTitle,cardSummery;

        public ViewHolder(final View itemView) {
            super(itemView);
            cardColor=itemView.findViewById(R.id.card_color);
            cardTitle=itemView.findViewById(R.id.card_title);
            cardSummery=itemView.findViewById(R.id.card_summery);
            cardIcon=itemView.findViewById(R.id.card_icon);
            cardInformation=itemView.findViewById(R.id.card_information);
        }
    }
}