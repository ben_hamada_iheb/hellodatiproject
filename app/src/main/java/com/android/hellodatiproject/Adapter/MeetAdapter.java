package com.android.hellodatiproject.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.hellodatiproject.R;
import com.android.hellodatiproject.UI.UI_AboutFragment;
import com.android.hellodatiproject.UI.UI_MeetingFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class MeetAdapter extends RecyclerView.Adapter<MeetAdapter.ViewHolder>{

    JSONArray data;
    Context context;

    public MeetAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_meet, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.roomName.setText(data.getJSONObject(i).getString("name"));
            viewHolder.roomArea.setText((Integer.parseInt(data.getJSONObject(i).getString("id"))*50)+" "+context.getString(R.string.area));
            viewHolder.roomNbrPerson.setText((Integer.parseInt(data.getJSONObject(i).getString("id"))*20)+" "+ context.getString(R.string.person));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_MeetingFragment.position=1;
                UI_MeetingFragment.tabs.setVisibility(View.VISIBLE);
                UI_MeetingFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_MeetingFragment.tabs.getTabAt(0).select();
                UI_MeetingFragment.setFragmentOfTab(new UI_AboutFragment());
                try {
                    UI_MeetingFragment.meetingTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView roomImage;
        TextView roomName,roomArea,roomNbrPerson;
        CardView itemCard;

        public ViewHolder(View itemView) {
            super(itemView);
            itemCard=itemView.findViewById(R.id.item_card);
            roomName=itemView.findViewById(R.id.room_name);
            roomArea=itemView.findViewById(R.id.room_area);
            roomNbrPerson=itemView.findViewById(R.id.room_nbr_person);
            roomImage=itemView.findViewById(R.id.room_image);

        }
    }
}
