package com.android.hellodatiproject.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.hellodatiproject.R;
import com.android.hellodatiproject.UI.UI_MenuFragment;

import org.json.JSONArray;
import org.json.JSONException;

public class ContentMenuAdapter extends RecyclerView.Adapter<ContentMenuAdapter.ViewHolder>{

    JSONArray data;
    Context context;


    public ContentMenuAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_menu_content, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        try {
            viewHolder.contentTitle.setText(data.getJSONObject(i).getString("name"));
            viewHolder.contentPrice.setText(data.getJSONObject(i).getString("id")+" Dt");
            viewHolder.contentSummery.setText(data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_MenuFragment.choosenContentLayout.setVisibility(View.VISIBLE);
                try {
                    UI_MenuFragment.choosenContentName.setText(data.getJSONObject(i).getString("name"));
                    UI_MenuFragment.choosenContentPrice.setText(data.getJSONObject(i).getString("id")+data.getJSONObject(i).getString("id")+" Dt");
                    UI_MenuFragment.choosenContentSummery.setText(data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name"));
                    UI_MenuFragment.choosenContentDescription.setText(data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name")+data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        /*viewHolder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UI_LeisureFragment.position=1;
                UI_LeisureFragment.tabs.setVisibility(View.VISIBLE);
                UI_LeisureFragment.buttomHeader.setVisibility(View.VISIBLE);
                UI_LeisureFragment.tabs.getTabAt(0).select();
                UI_LeisureFragment.setFragmentOfTab(new UI_AboutFragment());
                try {
                    UI_LeisureFragment.leisureTitle.setText(data.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout itemLayout;
        TextView contentTitle,contentPrice,contentSummery;


        public ViewHolder(View itemView) {
            super(itemView);
            itemLayout=itemView.findViewById(R.id.item_layout);
            contentTitle=itemView.findViewById(R.id.content_title);
            contentPrice=itemView.findViewById(R.id.content_price);
            contentSummery=itemView.findViewById(R.id.content_summery);

        }
    }
}