package com.android.hellodatiproject.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.hellodatiproject.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LangAdapter extends ArrayAdapter<LangAdapter.Lang> {

    Context context;
    String selected;
    // app language
    List<Lang> langs = Arrays.asList(
            new Lang("English", "en"),
            new Lang("Français", "fr"),
            new Lang("العربية", "ar")
    );

    public LangAdapter(@NonNull Context context) {
        super(context, android.R.layout.simple_spinner_item, new ArrayList<Lang>());
        this.context = context;

    }

    public void setSelected(String langIso) {
        selected = langIso;
    }

    public Lang getSelected() {
        for (int i = 0; i < langs.size(); i++) {
            if (langs.get(i).getIso().equals(selected)) {
                return langs.get(i);
            }
        }
        return langs.get(0);
    }

    @Override
    public int getCount() {
        return langs.size();
    }

    @Nullable
    @Override
    public Lang getItem(int position) {
        if (position >= 0 && position < langs.size()) {
            return langs.get(position);
        } else {
            return null;
        }
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = LayoutInflater.from(context).inflate(R.layout.lang_item, parent, false);
        Lang lang = langs.get(position);
        ((TextView) listItem.findViewById(R.id.name)).setText(lang.getName());
        ((TextView) listItem.findViewById(R.id.iso)).setText(lang.getIso());
        ImageView img_checked = listItem.findViewById(R.id.checked);
        if (lang.getIso().equals(getSelected().iso)) {
            img_checked.setImageResource(R.drawable.ic_valid_green);
        } else {
        }
        return listItem;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        Lang lang = langs.get(position);
        ((TextView) listItem.findViewById(android.R.id.text1)).setText(lang.getName());
        return listItem;
    }

    public class Lang {

        private String name, iso;

        public Lang(String name, String iso) {
            this.name = name;
            this.iso = iso;
        }

        public String getIso() {
            return iso;
        }

        public String getName() {
            return name;
        }
    }
}
