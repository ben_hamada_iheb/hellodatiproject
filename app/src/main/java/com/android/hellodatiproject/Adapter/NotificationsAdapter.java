package com.android.hellodatiproject.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.hellodatiproject.R;

import org.json.JSONArray;
import org.json.JSONException;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder>{

    JSONArray data;
    Context context;

    public NotificationsAdapter(JSONArray data, Context context){
        this.data=data;
        this.context=context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recycler_notification, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        if(i%2==0){
            viewHolder.newCard.setVisibility(View.GONE);
            viewHolder.notificationCard.setBackgroundResource(R.color.white);
        }
        else {
            viewHolder.dateTimeLayout.setVisibility(View.GONE);
            viewHolder.notificationCard.setBackgroundResource(R.color.offwhite);
        }
        try {
            viewHolder.notificationTitle.setText(data.getJSONObject(i).getString("name"));
            viewHolder.notificationPromo.setText(data.getJSONObject(i).getString("name"));
            viewHolder.notificationSummery.setText(data.getJSONObject(i).getString("name"));
            viewHolder.notificationDate.setText("22-11-2021");
            viewHolder.notificationTime.setText("22:25");
            viewHolder.notificationCategory.setText(data.getJSONObject(i).getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewHolder.notificationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send request to API to turn notif seen
                //and refresh
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout dateTimeLayout,notificationCard;
        CardView newCard;
        TextView notificationTitle,notificationPromo,notificationSummery,notificationDate,notificationTime,notificationCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            dateTimeLayout=itemView.findViewById(R.id.date_time_layout);
            newCard=itemView.findViewById(R.id.new_card);
            notificationTitle=itemView.findViewById(R.id.notification_title);
            notificationPromo=itemView.findViewById(R.id.notification_promo);
            notificationSummery=itemView.findViewById(R.id.notification_summery);
            notificationDate=itemView.findViewById(R.id.notification_date);
            notificationTime=itemView.findViewById(R.id.notification_time);
            notificationCategory=itemView.findViewById(R.id.notification_category);
            notificationCard=itemView.findViewById(R.id.notification_card);
        }
    }
}